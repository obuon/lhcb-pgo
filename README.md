## How to use super-project builds
- [Fork the project](https://gitlab.cern.ch/clemenci/lhcb-super-project-template/-/forks/new)
- Clone the project for locally
  ```
  git clone ssh://git@gitlab.cern.ch:7999/$USER/lhcb-super-project-template.git my-stack
  cd my-stack
  ```
- Declare the project you want to build, for example
  ```
  git submodule add ssh://git@gitlab.cern.ch:7999/lhcb/Gaudi.git
  git submodule add ssh://git@gitlab.cern.ch:7999/lhcb/Detector.git
  git submodule add ssh://git@gitlab.cern.ch:7999/lhcb/LHCb.git
  ```
- Follow the instructions for manual LHCb builds or a Visual Studio Code environment


## LHCb standard build
As for any CMake projects in LHCb we can build a full stack with something line:

```
lb-project-init
lb-set-platform x86_64_v2-centos7-gcc12-opt
export LCG_VERSION=103
make
```

Note that nothing is really built unless you add some project via `git submodule add <url>`.


## Visual Studio Code setup

### Plugins
The list of extensions to install is available in the "Workspace Recommandations" (`CTRL+P` then `Show Recommended Extensions`).

### Set up the CMake Kits in VSCode
In VSCode press `F1` and select the action `CMake: Edit User-Local CMake Kits`. When the editor opens a JSON file, add the content of `.vscode/cmake-tools-kits.json` to the list already present in the file. Save and close the file.

From the bottom bar select one of the new CMake Kits (e.g. `lcg-toolchains x86_64_v2-centos7-gcc11-opt (super-project)`), then you can build with `F7`.

Note that nothing is really built unless you add some project via `git submodule add <url>`.
